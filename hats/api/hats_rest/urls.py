from django.urls import path
from hats_rest.views import api_list_hats, api_detail_hats


urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path('hats/<int:pk>/', api_detail_hats, name='hat_detail')

    ]
