from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Hats, LocationVO
from common.json import ModelEncoder
# Create your views here.


class LoactionVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href",
                  "name", "id", ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["fabric",
                  "style",
                  "color",
                  "picture_url",
                  "location",
                  "id"
                ]
    encoders = {"location": LoactionVOEncoder()}




@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content['location'])
            print(location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            {"Hat": hat},
            encoder=HatsListEncoder
        )
    elif request.method == "DELETE":
        hat = Hats.objects.get(id=pk)
        hat.delete()
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        del content['id']
        Hats.objects.filter(id=pk).update(**content)
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatsListEncoder,
            safe=False,
        )
