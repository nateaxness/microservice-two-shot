import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()
from hats_rest.models import LocationVO


# Import models from hats_rest, here.
# from hats_rest.models import Something
# we have VO trying to populate datacreate or update a method with locationVO
# in ordeer to do that we need to get the actual locations
# once we have the locations we need to for loop for every location create a new VO location


def get_location():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={"name": location["closet_name"]},

        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_location()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
