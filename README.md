# Wardrobify

Team:

* Marcus Der - Hat Microservice
* Nate Axness - Shoes Microservice

## Design

## Shoes microservice

Created a Shoes model to have the specified properties and it have a foreignkey to location.
Then created a view functions to handle certain request such as PUT, DELETE, POST, GET.
Made corresponding js files to handle each of those desired functionalities.
Then linked the js files to the App.js file via routes to have them appear as a single application program.

## Hats microservice

Created a Hats model to have the specified properties and it have a foreignkey to location. Then created a view functions to handle certain request such as PUT, DELETE, POST, GET. Made corresponding js files to handle each of those desired functionalities. Then linked the js files to the App.js file via routes to have them appear as a single application program.
