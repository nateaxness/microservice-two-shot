import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatList from './HatsList';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/create" element={<HatForm />} />
          <Route path="/shoes/create" element={<ShoesForm />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/hats" element={<HatList />} />


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
