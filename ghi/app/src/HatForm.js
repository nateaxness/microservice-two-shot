import React, { useState, useEffect } from 'react';

function HatForm() {
  const [style, setStyle] = useState('');
  const [color, setColor] = useState('');
  const [picture_url, setPicture] = useState('');
  const [fabric, setFabric] = useState('');
  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState('');

  async function fetchLocations() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    console.log(data)
    }
  }

  useEffect(() => {
    fetchLocations();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {}
    data.style = style
    data.color = color
    data.picture_url = picture_url
    data.fabric = fabric
    data.location = location
    

    const hatsUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setStyle('');
      setColor('');
      setPicture('');
      setFabric('');
      setLocation('');
    }
  }

  function handleChangeStyle(event) {
    const { value } = event.target;
    setStyle(value);
  }

  function handleChangeColor(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handleChangePicture(event) {
    const { value } = event.target;
    setPicture(value);
  }

  function handleChangeFabric(event) {
    const { value } = event.target;
    setFabric(value);
  }

  function handleChangeLocation(event) {
    const { value } = event.target;
    setLocation(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input value={style} onChange={handleChangeStyle} placeholder="style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleChangeColor} placeholder="color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={handleChangePicture} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="mb-3">
              <label htmlFor="fabric">Fabric</label>
              <input value={fabric} onChange={handleChangeFabric} className="form-control" id="fabric" name="fabric"></input>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
