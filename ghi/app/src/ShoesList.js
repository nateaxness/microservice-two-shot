import React, { useState, useEffect } from 'react';
import App from './App';


function ShoesList(props) {
  const [shoes, setShoes] = useState([])
  // if (props.shoes === undefined) {
  //   return null;
  // }
  async function loadShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    } else {
      console.log(response);
    }
  }

  const handleUpdate = async (shoeId) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
      method: "PUT",
    });
    if (response.ok) {
      loadShoes() // calls on loadShoes function to recheck database as source of truth
    } else {
      console.error('failed to update shoe:', await response.text());
    }
  }

  const handleDelete = async (shoeId) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
      method: "DELETE",
    });
    if (response.ok) {
      loadShoes() // calls on loadShoes function to recheck database as source of truth
      // setShoes(shoes.filter(shoe => shoe.id !== shoeId));
    } else {
      console.error('failed to delete shoe:', await response.text());
    }
  }

  useEffect(() => {
    loadShoes();
  }, [])

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.name }</td>
                {/* <td><img src ={ hat.picture_url } width={90} height={90}/></td> */}
                <td>
                  <button onClick={() => handleUpdate(shoe.id)}>Update Shoe</button>
                </td>
                <td>
                  <button onClick={() => handleDelete(shoe.id)}>Delete Shoe</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoesList;
