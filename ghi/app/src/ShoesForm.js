import React, { useEffect, useState } from 'react';

function ShoesForm(props) {
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModel] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
      }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data);

        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
          const newShoes = await response.json();
          console.log(newShoes);

          setManufacturer('');
          setModel('');
          setColor('');
          setPictureUrl('');
          setBin('');
        }
      }


    const [bins, setBins] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setBins(data.bins)
        console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create new Shoes</h1>
              <form onSubmit={handleSubmit} id="create-shoes-form">
                <div className="form-floating mb-3">
                  <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="name" className="form-control" />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleModelChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                  <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handlePictureUrlChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"></textarea>
                  <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleBinChange} required type="text" id="bin" name="bin" className="form-select">
                    <option defaultValue value="">Choose a Bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

export default ShoesForm;
