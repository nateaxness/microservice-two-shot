import React,{useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

function HatList(){
    const [hats, setHats] = useState([]);

    async function loadHats() {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setHats(data.hats);
        } else {
            console.log(response)
        }
      }



    const handleDelete = async (hatId) => {
    const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
        method: "DELETE",
    });
    if (response.ok) {
        loadHats()
        // setHats(hats.filter(hat => hat.id !== hatId));
    } else {
        console.error('failed to delete shoe:', await response.text());
    }
    }

      useEffect(() => {
        loadHats();
      }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>color</th>
            <th>fabric</th>
            <th>style</th>
            <th>picture_url</th>
            <th>location</th>
            <th>link to buy</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.color }</td>
                <td>{ hat.fabric}</td>
                <td>{ hat.style }</td>
                <td><img src={ hat.picture_url } alt="pictures" height = {150} width ={150}/></td>
                <td>{ hat.location.name }</td>
                <td><Link to="https://www.youtube.com/watch?v=xvFZjo5PgG0">Buy this here!</Link></td>
                <td>
                  <button onClick={() => handleDelete(hat.id)}>Delete Hat</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }


  export default HatList;
